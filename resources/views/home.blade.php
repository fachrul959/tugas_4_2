@extends('layouts.main')
@section('container')

@if (session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<div class="d-flex justify-content-between flex-wrap">

    @foreach ($movies as $movie)
    <div class="card mb-4" style="width: 18rem;">
        <img src="{{ $movie['cover_img'] }}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">{{ $movie['title'] }}</h5>
            <p class="card-text">{{$movie['description']}}</p>
            {{-- <a href="#" class="btn btn-primary">Get Ticket</a> --}}
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#bookModal"
                    data-bs-id="{{ $movie['id'] }}" data-bs-title="{{ $movie['title'] }}">
                Get Ticket
            </button>
        </div>
    </div>
    @endforeach
    <!-- Modal -->
    <div class="modal fade" id="bookModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="movieTitle">{{ $movie['title'] }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" class="form-control"
                               value="{{ auth()->user()->id }}">
                        <input type="hidden" name="movie_id" class="form-control"
                               value="{{$movie['id']}}">
                        <div class="mb-3">
                            <label for="date" class="form-label">Date</label>
                            <input type="date" name="date" class="form-control" id="date">
                        </div>
                        <div class="mb-3">
                            <label for="time" class="form-label">Time</label>
                            <input type="time" name="time" class="form-control" id="time">
                        </div>
                        <div class="mb-3">
                            <label for="qty" class="form-label">Quantity</label>
                            <input type="number" name="qty" class="form-control" id="qty">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                    data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Book Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Akhir Modal -->

</div>

<script>
    var bookModal = document.getElementById('bookModal')
    bookModal.addEventListener('show.bs.modal', function (event) {
        var button = event.relatedTarget
        var id = button.getAttribute('data-bs-id')
        var title = button.getAttribute('data-bs-title')
        var movieTitle = bookModal.querySelector('.modal-title')
        var movieId = bookModal.querySelector('input[name="movie_id"]')
        movieTitle.textContent = title
        movieId.value = id
    })

</script>

@endsection
