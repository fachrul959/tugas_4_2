<header>
    <div class="px-3 py-2 border-bottom mb-3">
        <div class="container d-flex flex-wrap justify-content-between">
            <div>
                <a href="/" class="navbar-brand link-dark">Movies</a>
            </div>
            @auth
            <form action="/logout" method="POST">
                @csrf
                <button type="submit" class="btn btn-light text-dark me-2">Logout</button>
            </form>
            @else
            <div>
                <a href="/login" class="btn btn-light text-dark me-2">Login</a>
                <a href="/register" class="btn btn-primary">Sign-up</a>
            </div>
            @endauth
        </div>
    </div>
</header>
