@extends('layouts.main')
@section('container')

<div class="row justify-content-center">
    <div class="col-md-4 form-signup">
        <form action="/register" method="POST">
            @csrf
            <h1 class="h3 my-3 fw-normal text-center">Sign Up</h1>
            <div class="form-floating">
                <input type="text" class="form-control rounded-top" name="name" id="name" placeholder="Name">
                <label for="name">Name</label>
            </div>
            <div class="form-floating">
                <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com">
                <label for="email">Email address</label>
            </div>
            <div class="form-floating">
                <input type="password" name="password" class="form-control rounded-bottom" id="password"
                       placeholder="Password">
                <label for="password">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">Create Account</button>
        </form>

    </div>


    @endsection
