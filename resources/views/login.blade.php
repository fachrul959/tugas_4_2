@extends('layouts.main')
@section('container')

<div class="row justify-content-center">
    <div class="col-md-4 form-signin">
        <form action="/login" method="POST">
            @csrf
            <h1 class="h3 my-3 fw-normal text-center">Login</h1>
            <div class="form-floating">
                <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com"
                       autofocus required>
                <label for="email">Email address</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password"
                       required>
                <label for="password">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">Login</button>
        </form>
        <small class="text-center d-block mt-2">Not Registered? <a class="text-decoration-none" href="/register">Sign-up
                now</a></small>

    </div>


    @endsection
